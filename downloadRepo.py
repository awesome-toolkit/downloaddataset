from TABICON27.MyListArgs import *
import os, sys, subprocess

downloadTo = ''

def callScript(script, arguments):
    p = subprocess.Popen(script+arguments, stdout=subprocess.PIPE, shell=True)
    out, err = p.communicate() 
    result = out.split('\n')
    out = ''
    for lin in result:
        if not lin.startswith('#'):
            out = out+lin
    return out

def createDirs(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory, 0777)
    except OSError as err:
        print(err)

def downloadDataset(url, downloadTo):
    out = callScript('git',' ls-remote '+url)
    if downloadTo != '':
        createDirs(downloadTo)

    if out in 'fatal':
        if downloadTo != '':
            downloadTo = '-P '+downloadTo
        callScript('wget',' -q -o /dev/null '+url+' '+downloadTo)
    else:
        callScript('git',' clone '+url+' '+downloadTo)


if __name__ == "__main__":
    args = []
    for element in range(1,len(sys.argv)):
        args.append(sys.argv[element])
 
    param = MyListArgs(args)
 
    configFile = param.valueArgsAsString("-CONFIG","")
    if configFile != "":
        param.addArgsFromFile(configFile)
 
    sintaxis = "-REPO:str [-DOWNLOAD_TO:str]"
 
    repo       = param.valueArgsAsString('-REPO'       , '')
    downloadTo = param.valueArgsAsString('-DOWNLOAD_TO', '') 

    downloadDataset(repo, downloadTo)